<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class PageController extends Controller
{
    //
    public function __construct()
    {
        $this->middleware('authcustom');
    }

    public function index()
    {
        return view('index');
    }
}
